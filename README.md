# IOTA Seed Generator as Docker Image

Create a valid and secure IOTA seed with this docker image.

Repository: https://gitlab.com/cyancor/docker-iota-seed-generator

[![pipeline status](https://gitlab.com/cyancor/docker-iota-seed-generator/badges/master/pipeline.svg)](https://gitlab.com/cyancor/docker-iota-seed-generator/commits/master)