FROM alpine:latest

LABEL maintainer="CyanCor GmbH - https://cyancor.com/"

ENTRYPOINT cat /dev/urandom | tr -dc A-Z9 | head -c${1:-81} && echo